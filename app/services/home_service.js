/**
 * Created by anil on 11.08.2016.
 */
'use strict';

angular.module('myApp.home')
    .service('IndexService', ['$http',function($http) {
        var IndexService = {};
        var url = 'http://127.0.0.1:9200/omdb/_search/';

        IndexService.getMovies = function(query) {
            return $http.post(url, query);
        };

        return IndexService;
    }]);

