'use strict';

angular.module('myApp.home', []);


// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.home',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/view2'});
  $routeProvider
      .when('/view2',{
        templateUrl : 'templates/home.html',
        controller : 'View2Ctrl'
      })

      .when('/stats/',{
          templateUrl : 'templates/stats.html'
      });





}]);
