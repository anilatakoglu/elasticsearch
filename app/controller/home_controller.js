'use strict';

angular.module('myApp.home')
    .controller('View2Ctrl', ['$scope', 'IndexService', function($scope, IndexService) {
        $scope.show = false;

        $scope.getFilm =function(){

            var query = '{"query":{"match_all":{}},"size":18}';
            IndexService.getMovies(query)
                .success( function (data) {
                    $scope.movie = data;
                    $scope.show=false;
                });

        };
        $scope.getFilm();

        $scope.getFilter =function(genre){
            $scope.genre = angular.copy(genre);
            var query = '{"query":{"bool":{"must":[{"term":{"Genre":"'+$scope.genre+'"}}],"must_not":[],"should":[]}},"from":0,"size":18}';
            IndexService.getMovies(query)
                .success( function (data) {
                    $scope.movie = data;
                });

        };

        $scope.overSeven =function(){
            var query = '{"query":{"bool":{"must":[{"range":{"imdbRating":{"gt":"7","lt":"9.9"}}}],"must_not":[],"should":[]}},"from":0,"size":18}';
            IndexService.getMovies(query)
                .success( function (data) {
                    $scope.movie = data;
                });

        };

        $scope.getSearch =function(title){
            $scope.title = angular.copy(title);
            if($scope.title==""){
                $scope.getFilm();
            } else {
                var query = '{"query":{"bool":{"must":[{"fuzzy":{"Title":"'+$scope.title+'"}}],"must_not":[],"should":[]}},"from":0,"size":18}';
                IndexService.getMovies(query)
                    .success( function (data) {
                        $scope.movie = data;
                    });
            }
        };

        $scope.getYear=function(date){
            $scope.date = angular.copy(date);
            //$scope.show = true;

            var query = '{"query":{"bool":{"must":[{"term":{"Year":"'+$scope.date+'"}}],"must_not":[],"should":[]}},"from":0,"size":18}';
            IndexService.getMovies(query)
                .success( function (data) {
                    $scope.movie = data;

                });
        };

        $scope.getRelavantFilm =function(id){
            $scope.id = angular.copy(id);
            $scope.show = true;

            var query = '{"query":{"bool":{"must":[{"term":{"ID":"'+$scope.id+'"}}],"must_not":[],"should":[]}},"from":0,"size":18}';
            IndexService.getMovies(query)
                .success( function (data) {
                    $scope.movie = data;

                });

        };



    }]);